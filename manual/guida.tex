\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[italian]{babel}
\usepackage{color}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{listings}

\definecolor{mygray}{RGB}{219,219,219}

\lstnewenvironment{codeenv}{%
  \lstset{backgroundcolor=\color{mygray},
  literate={~} {$\sim$}{1},
  frame=single,
  framerule=0pt,
  basicstyle=\ttfamily,
  columns=fullflexible,
  breaklines=true}}{}

%opening
\title{Una guida per la gestione di un repository \textsc{Bib}\TeX centralizzato}
\author{Giuseppe Cota}

\newcommand{\code}[1]{\colorbox{mygray}{\texttt{#1}}}

\begin{document}

\maketitle

\section{Introduzione}
\textsc{Bib}\TeX è uno strumento largamente diffuso per rappresentare le informazioni bibliografiche da citare in un articolo o un libro. Esse sono contenute dentro file con estensione \texttt{.bib}. Per ogni nuovo paper dovremmo scrivere le informazioni bibliografiche, spesso tuttavia molte di queste informazioni sono già referenziate in un paper precedente e quindi invece di riscriverle si fa un copia e incolla (o di tutto il file o solo delle parti necessarie) e si aggiungono nuove bibliografie. Questo procedimento è molto spesso noioso e poco flessibile. Sarebbe comodo avere un repository centrale in continuo aggiornamento contenente tutte le bibliografie di cui necessitiamo.

Si vuole proporre un approccio per gestire le referenze in un unico repository bibliografico \textsc{Bib}\TeX. Per farlo è necessario mettere i file \texttt{.bib} in un repository Git ed utilizzarlo come submodule (submodulo) dei repository Git dei paper.

La stategia è basata su~\cite{Velykis:2012:SMBFUGS}. Vedremo qui i comandi principali da utilizzare per avere un repository bibliografico centralizzato, remoto, aggiornabile ed allo stesso tempo coerente con i vari progetti di paper.

Essenzialmente quello che vogliamo è soddisfare le seguenti condizioni:
\begin{enumerate}
 \item Avere un repository centrale per la nostra bibliografia continuamente aggiornato.
 \item Avere un repository centrale accessibile da remoto.
 \item Le referenze\textsc{Bib}\TeX (i file \texttt{.bib}) devono essere ``versionati''. Per ogni progetto di paper (quindi per ogni repository contenente i file \texttt{.tex}) voglio al suo interno un submodule contenente una versione della bibliografia che sia coerente con le key utilizzate nel paper. Quando modifico un paper scritto tempo prima voglio avere le stesse chiavi/referenze utilizzate quando l'ho scritto inizialmente. Il repository bibliografico centrale protrebbe essere stato aggiornato e quindi delle chiavi possono essere cambiate. Mentre io voglio avere nel repository del mio paper una specifica versione della mia bibliografia.
\end{enumerate}
  

\section{Aggiungere in locale il repository centrale}
Per aggiungere un submodule in Git è necessario utilizzare il comando \code{git
submodule add} con l'URL del repository remoto contenente la nostra bibliografia. Nel nostro caso come submodulo vogliamo aggiungere come submodulo il repository Git che contiene i file \texttt{.bib}, ossia la nostra bibliografia. Quindi useremo il comando:
\begin{codeenv}
 git submodule add git@bitbucket.org:machinelearningunife/bibtexrepository.git [<path>]
\end{codeenv}
Di default con questo comando il submodulo viene inserito in una cartella che ha lo stesso nome del repository, in questo caso ``bibtexrepository'' ma è possibile assegnare un path differente.
Esempio:
\begin{codeenv}
 :~/edgempi\$ git submodule add git@bitbucket.org:machinelearningunife/bibtexrepository.git bibliography
\end{codeenv}

\section{Clonare il progetto}
Per clonare un progetto con un submodulo in git è necessario utilizzare il comando \code{git clone -{}-recursive} con l'URL del repository remoto contenente il progetto principale, ossia il repository contenente il paper e poi all'interno della cartella contenente il submodulo fare un checkout (solitamente sul master)\footnote{Fare il checkout sul master è necessario per effettuare dei commit e dei push sul repository bibliografico direttamente dal submodulo. Infatti quando facciamo un \code{git clone -{}-recursive} il nostro submodulo è in ``detached HEAD'' state. per maggiori informazioni si veda~\cite{Chacon:2009:PG:1618548}}. 

Ad esempio:
\begin{codeenv}
:~\$ git clone --recursive git@bitbucket.org:giuseta/2015ilp_edgempi.git
:~\$ cd 2015ilp_edgempi
:~/2015ilp_edgempi\$ cd bibliography
:~/2015ilp_edgempi/bibliography\$ git checkout master
\end{codeenv}
In questa maniera abbiamo una copia locale del nostro repository e della versione opportuna del submodulo.

\section{Lavorare sul repository contenente il paper ed il submodulo}
Abbiamo visto come ottenere la copia del progetto principale contenente un submodulo\footnote{Ripetizione: il submodulo nel nostro caso è sempre il progetto contenente i file \texttt{.bib}}

\subsection{Aggiornare il progetto paper a cui è stato aggiunto il submodulo bibliografico}
Se un collaboratore ha aggiunto il submodulo bibliografico e noi abbiamo già in locale il nostro progetto senza il submodulo, allora bisogna eseguire il pull del progetto, poi invocare \code{git submodule init} e \code{git submodule update}. Adesso il submodulo è in uno stato di ``detached HEAD'' quindi bisogna andare nella cartella del submodulo e fare un checkout (solitamente sul master). 

Ad esempio:
\begin{codeenv}
:~/2015ilp_edgempi\$ git pull
:~/2015ilp_edgempi\$ git submodule init
:~/2015ilp_edgempi\$ git submodule update
:~/2015ilp_edgempi\$ cd bibliography
:~/2015ilp_edgempi/bibliography\$ git checkout master
\end{codeenv}

\subsection{Aggiornare il progetto paper}
Se vogliamo aggiornare il progetto paper ed il sottomodulo bibliografico associato bisogna utilizzare i comandi \code{git pull} e \code{git submodule update}.

Esempio:
\begin{codeenv}
:~/2015ilp_edgempi\$ git pull
:~/2015ilp_edgempi\$ git submodule update
:~/2015ilp_edgempi\$ cd bibliography
:~/2015ilp_edgempi/bibliography\$ git checkout master
\end{codeenv}

\subsection{Update del submodulo}
Se il nostro repository bibliografico è stato modificato e vogliamo la versione più recente in assoluto, allora possiamo aggiornare il submodulo. Per farlo dobbiamo invocare il comando \code{git submodule update -{}-remote -{}-merge [<path>]}\footnote{Funziona con versioni di Git $\geq$ 1.8.2}. Se non è stato già fatto è necessario fare prima un checkout dentro la cartella contenente il submodulo.

Esempio:
\begin{codeenv}
:~/2015ilp_edgempi\$ cd bibliography
:~/2015ilp_edgempi/bibliography\$ git checkout master
:~/2015ilp_edgempi\$ cd ..
:~/edgempi\$ git submodule update --remote --merge bibliography/
remote: Counting objects: 5, done.
remote: Compressing objects: 100\% (5/5), done.
remote: Total 5 (delta 1), reused 0 (delta 0)
Unpacking objects: 100\% (5/5), done.
From bitbucket.org:giuseta/bibtexrepository
   cf4b3c5..2a82b52  master     -> origin/master
Submodule path 'bibliography': checked out '2a82b527d17e59d77d65d6e01056a05cfad1b62a'
\end{codeenv}


%Se tuttavia è stata fatta una modifica locale al submodulo ed è stato fatto un commit
\section{Pubblicare le modifiche fatte nel repository}
% Se è stata fatta una modifica al submodule oltre che ai file principali del paper bisogna eseguire il comando:
% \begin{codeenv}
%  git push --recurse-submodules=on-demand
% \end{codeenv}

Eseguire come si fa di solito \code{commit} e \code{push}.% È possibile eseguire dei commit sul repository bibliografico direttamente dal repository del paper con dei \texttt{push} partcolari, ma per brevità non verranno esposti. Per approfondimenti leggere~\cite{Chacon:2009:PG:1618548}. Quindi se si vuole aggiornare 

\section{Ulteriori Informazioni}
Per ulteriori informazioni riguardanti i \emph{submodule} in Git leggere~\cite{Chacon:2009:PG:1618548}

\begin{thebibliography}{1}
 \bibitem{Velykis:2012:SMBFUGS}
 Velykis, Andrius:
 Strategy for master BibTeX file using Git submodules.
 Link: http://andrius.velykis.lt/2012/06/master-bibtex-file-git-submodules/.
 2012
 
 \bibitem{Chacon:2009:PG:1618548}
 Chacon, Scott:
 Pro Git.
 Link: http://git-scm.com/book/en/v2/Git-Tools-Submodules
\end{thebibliography}


\end{document}
